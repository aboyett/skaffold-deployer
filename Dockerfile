FROM alpine:3.11 AS builder

RUN apk add --no-cache curl make tar

COPY Makefile ./

RUN make build

FROM library/docker:stable

RUN apk add --no-cache git jq tar

COPY --from=builder helm kubectl skaffold /usr/local/bin/
