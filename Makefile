HELM_VERSION ?= v3.1.0
KUBECTL_VERSION ?= v1.16.4
SKAFFOLD_VERSION ?= v1.5.0

HELM_URL ?= https://get.helm.sh/helm-$(HELM_VERSION)-linux-amd64.tar.gz
KUBECTL_URL ?= https://dl.k8s.io/$(KUBECTL_VERSION)/kubernetes-client-linux-amd64.tar.gz
SKAFFOLD_URL ?= https://github.com/GoogleContainerTools/skaffold/releases/download/${SKAFFOLD_VERSION}/skaffold-linux-amd64

TOPDIR := $(CURDIR)
TMPDIR := tmp

# https://get.helm.sh/helm-v3.0.2-linux-amd64.tar.gz
# https://dl.k8s.io/v1.16.4/kubernetes-client-linux-amd64.tar.gz
# https://storage.googleapis.com/skaffold/releases/latest/skaffold-linux-amd64

helm: 
	mkdir -p $(TMPDIR)
	( cd $(TMPDIR) && \
		curl -Lo helm.tar.gz $(HELM_URL) && \
		tar xzf helm.tar.gz --strip=1 -C $(TOPDIR) linux-amd64/helm \
	)

kubectl:
	mkdir -p $(TMPDIR)
	( cd $(TMPDIR) && \
		curl -Lo kubectl.tar.gz $(KUBECTL_URL) && \
		tar xzf kubectl.tar.gz --strip=3 -C $(TOPDIR) kubernetes/client/bin/kubectl \
	)

skaffold:
	curl -Lo skaffold $(SKAFFOLD_URL)
	chmod +x skaffold

build: helm kubectl skaffold
	rm -r $(TMPDIR)
